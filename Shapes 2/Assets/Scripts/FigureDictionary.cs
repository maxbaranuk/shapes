﻿using UnityEngine;
using System.Collections;

public static class FigureDictionary {

    public static Figure[] figures = { new Figure(new Vector3[] { new Vector3(0, 0, 0),
                                                                  new Vector3(120, 200, 0),
                                                                  new Vector3(240, 0, 0) }),

                                       new Figure(new Vector3[] { new Vector3(0, 0, 0),
                                                                  new Vector3(0, 200, 0),
                                                                  new Vector3(200, 200, 0),
                                                                  new Vector3(200, 0, 0)}),

                                       new Figure(new Vector3[] { new Vector3(0, 0, 0),
                                                                  new Vector3(0, 105, 0),
                                                                  new Vector3(210, 105, 0),
                                                                  new Vector3(210, 0, 0)}),

                                        new Figure(new Vector3[] { new Vector3(0, 128, 0),
                                                                  new Vector3(90, 0, 0),
                                                                  new Vector3(90, 256, 0),
                                                                  new Vector3(180, 128, 0)})
    };
}
