﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public GameObject score;
	void Start () {
        Settings.bestScore = Settings.load();
        score.GetComponent<Text>().text = "Лучший результат: " + Settings.bestScore;

    }
	
	void Update () {
	}

    public void startGame() {
        SceneManager.LoadScene(1);
    }
}
