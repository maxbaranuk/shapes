﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main : MonoBehaviour {

    public GameObject trail;
    public GameObject image;
    public GameObject losePanel;
    public GameObject score;

    Figure figure;
    Vector3 startSidePoint;
    Vector3 prevPoint;
    int counter = 0;
    Vector3 sideDir;
    Vector3 prevSideDir;
    bool isFirst;
    float mouseSensitivity = 0.3f;   
    Figure example; 
    float timeToLose = Settings.timeToDraw;

    void Start () {
        int type = Random.Range(0, FigureDictionary.figures.Length);
        Debug.Log("Type: " + type);
        example = FigureDictionary.figures[type];
        Sprite figSprite = Resources.Load<Sprite>("images/" + type);
        image.GetComponent<Image>().sprite = figSprite;
    }
	
	void Update () {
        drawFigure();
    }

    void loseGame() {
        score.GetComponent<Text>().text = "Результат: " + Settings.score;
        losePanel.SetActive(true);
        Settings.save(Settings.score);
    }

    public void restart() {
        Settings.score = 0;
        Settings.timeToDraw = 3;
        SceneManager.LoadScene(1);
    }

    void drawFigure() {
        if (Input.GetMouseButtonDown(0))
        {
            figure = new Figure();
            startSidePoint = Input.mousePosition;
            prevPoint = startSidePoint;
            prevSideDir = new Vector3();
            counter = 0;
            trail.SetActive(true);
        }

        if (Input.GetMouseButton(0))
        {
            timeToLose -= Time.deltaTime;
            if (timeToLose < 0) loseGame();

            counter++;
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint((Input.mousePosition)), Vector2.zero);
            if (hit.collider != null) trail.transform.position = hit.point;

            if (counter % 10 == 0)
            {
                counter = 0;
                Vector3 currPos = Input.mousePosition;

                if (isFirst)
                {
                    prevSideDir = prevPoint - currPos;
                    isFirst = false;
                }
                else {
                    sideDir = prevPoint - currPos;
                    if (Mathf.Abs(Vector3.Angle(sideDir, prevSideDir)) > 20 && prevSideDir.magnitude > 30
                        && (Mathf.Abs(Input.GetAxis("Mouse X")) > mouseSensitivity || Mathf.Abs(Input.GetAxis("Mouse Y")) > mouseSensitivity))
                    {
                        figure.points.Add(prevPoint);
                        prevSideDir = sideDir;
                        startSidePoint = prevPoint;
                    }
                    else prevSideDir = startSidePoint - currPos;
                    
                }
                prevPoint = currPos;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector3 currPos = Input.mousePosition;
            figure.points.Add(currPos);
            if (Figure.checkForSame(figure, example))
            {
                Settings.score++;
                Settings.timeToDraw -= 0.05f;
                SceneManager.LoadScene(1);
            }
            timeToLose = Settings.timeToDraw;
//            else loseGame();

        }

    }
}
