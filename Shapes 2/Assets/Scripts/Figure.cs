﻿using UnityEngine;
using System.Collections.Generic;

public class Figure {

    public List<Vector3> points;

    public Figure() {
        points = new List<Vector3>();
    }
    public Figure(params Vector3[] points)
    {
        this.points = new List<Vector3>(points);
    }

    public static bool checkForSame(Figure f, Figure f2) {
        bool isSame = true;
        List<Vector3> one = makeVectors(f);
        List<Vector3> two = makeVectors(f2);
        if (one.ToArray().Length != two.ToArray().Length) isSame = false;
        else {
            float koef = one[0].magnitude / two[0].magnitude;
            for (int i = 0; i < one.ToArray().Length; i++)
            {
                float currKoef = one[i].magnitude / two[i].magnitude;
                koef = (koef + currKoef) / 2;
                if (currKoef / koef > 1.15f || currKoef / koef < 0.85f) {
                    isSame = false;
                    Debug.Log("koeff "+currKoef / koef);
                } 
                if (Mathf.Abs(dirAngle(one[i]) - dirAngle(two[i])) > 15)
                {
                    isSame = false;
                    Debug.Log("Angle " + Mathf.Abs(dirAngle(one[i]) - dirAngle(two[i])));
                    break;
                }
            }
        }       
        return isSame;
    }

    static List<Vector3> makeVectors(Figure f) {
        List<Vector3>  vectors = new List<Vector3>();
        Vector3 center = new Vector3();
        foreach (Vector3 v in f.points)
        {
            center += v;
        }
        center /= f.points.ToArray().Length;

        foreach (Vector3 v in f.points)
        {
            vectors.Add(center - v);
        }
        sortByAngle(vectors);
        
        return vectors;
    }

    static void sortByAngle(List<Vector3> vect) {
        for (int i = vect.ToArray().Length - 1; i > 0; i--)
        {
            for (int j = 0; j < i; j++)
            {               
                if (dirAngle(vect[j]) > dirAngle(vect[j+1]) ){
                   Vector3 tmp = vect[j];
                    vect[j] = vect[j+1];
                    vect[j+1] = tmp;
            }
        }
    }
    }

    static float dirAngle(Vector3 v) {
        float angle = Vector3.Angle(v, Vector3.up);
        float sign = Mathf.Sign(Vector3.Dot(Vector3.forward, Vector3.Cross(v, Vector3.up)));
        float signed_angle = angle * sign;
        return signed_angle;
    }
}
